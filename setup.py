from setuptools import setup, find_packages

setup(
    name="libprologin",
    author="Association Prologin",
    url="https://bitbucket.org/prologin/libprologin",
    packages=find_packages(),
    install_requires=['requests'],
    tests_require=['pytest', 'pytest-cov'],
    classifiers=[
        'Intended Audience :: Developers',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
    ]
)
