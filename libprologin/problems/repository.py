from pathlib import Path

from libprologin.contest import EventType
from libprologin.problems.challenge import Challenge


class Repository:
    def __init__(self, path):
        self.root = Path(path)

    @staticmethod
    def parse_challenge_name(name: str):
        for event_type, keyword in Challenge.event_type_to_dirname.items():
            if name.startswith(keyword):
                break
        else:
            raise ValueError("invalid name: {}".format(name))
        year = int(name[len(keyword):])
        return year, event_type

    def challenges(self):
        for path in self.root.iterdir():
            try:
                year, event_type = self.parse_challenge_name(path.name)
            except ValueError:
                continue
            yield Challenge(self.root, year, event_type)

    def challenge_by_name(self, name: str):
        return self.challenge_by_type_year(*self.parse_challenge_name(name))

    def challenge_by_type_year(self, year: int, event_type: EventType):
        return Challenge(self.root, year, event_type)
