import contextlib
import enum
import subprocess
import tempfile
from contextlib import ExitStack
from pathlib import Path

from libprologin import problems
from libprologin.language import Language
from libprologin.utils import cached_property


class TestType(enum.Enum):
    correction = 'correction'
    performance = 'performance'


class Test:
    def __init__(self, name: str, input: str, output: str, type: TestType,
                 hidden: bool):
        self.name = name
        self.input = input
        self.output = output.strip()
        self.type = type
        self.hidden = hidden

    def is_valid(self, output: str):
        return self.output.strip() == output.strip()

    def __repr__(self):
        return '<{} {} {} ({})>'.format(
            self.__class__.__name__, self.name, self.type.name,
            'hidden' if self.hidden else 'visible')


class Sample:
    def __init__(self, input: str, output: str, comment: str):
        self.input = input
        self.output = output
        self.comment = comment

    def __repr__(self):
        return '<{} {}>'.format(self.__class__.__name__, self.comment[:16])


class Problem(problems.Container):
    properties_filename = 'problem.props'

    def __init__(self, challenge, name: str):
        self.challenge = challenge
        self.name = name
        self.root = self.challenge.root / self.name
        if not self.root.is_dir():
            raise FileNotFoundError("no such problem: {}".format(self.root))

    def __repr__(self):
        return '<{}: {} in {!r}>'.format(
            self.__class__.__name__, self.name, self.challenge)

    def __hash__(self):
        return hash(self.root)

    def __eq__(self, other):
        return self.root == other.root

    def __lt__(self, other):
        return (self.difficulty, self.title) < (other.difficulty, other.title)

    @property
    def title(self) -> str:
        return self.properties.get('title', '')

    @property
    def difficulty(self) -> int:
        return self.properties.get_int('difficulty', 0)

    @property
    def stop_early(self) -> bool:
        return self.properties.get_bool('stop-early', True)

    @cached_property
    def subject_html(self) -> str:
        try:
            return self.read_file('subject.txt')
        except FileNotFoundError:
            pass

    @cached_property
    def subject_markdown(self) -> str:
        try:
            return self.read_file('subject.md')
        except FileNotFoundError:
            pass

    @property
    def subject(self) -> str:
        return self.subject_markdown or self.subject_html

    @property
    def maximum_memory(self) -> int:
        """Maximum memory allowed, in megabytes."""
        return self.properties.get_int('mem')

    @property
    def maximum_time(self) -> float:
        """Maximum time allowed, in seconds."""
        return self.properties.get_int('time') / 1000.

    @cached_property
    def references(self):
        for ref in self.root.glob('ref.*'):
            lang = Language.guess(ref.suffix)
            if lang is not None:
                yield (lang, ref)

    @cached_property
    def _test_dict(self):
        return {test.name: test for test in self.tests}

    def test_by_name(self, name: str):
        return self._test_dict[name]

    @cached_property
    def tests(self):
        """The problem tests."""
        # The intersection of *.in and *.out having the same name ('stem')
        # NOTE: don't use .with_suffix(), it removes all dotted parts
        stem_key = lambda p: p.stem
        hidden_tests = set(self.properties.get_list('hidden'))
        performance_tests = set(self.properties.get_list('performance'))
        paths_in = sorted(self.root.glob('*.in'), key=stem_key)
        names_out = set(map(stem_key, self.root.glob('*.out')))
        for path_in in paths_in:
            name = path_in.stem
            if name in names_out:
                path_out = (self.root / (name + '.out'))
                yield Test(
                    name,
                    self.read_file(path_in),
                    self.read_file(path_out),
                    TestType.performance if name in performance_tests
                    else TestType.correction,
                    name in hidden_tests)

    def _missing_test(self, names):
        # Utility function for _missing_test_*()
        return set(names) - {test.name for test in self.tests}

    @property
    def _missing_test_in(self):
        """Debug utility: tests having *.out file but no .in file"""
        return self._missing_test(p.stem for p in self.root.glob('*.out'))

    @property
    def _missing_test_out(self):
        """Debug utility: tests having *.in file but no .out file"""
        return self._missing_test(p.stem for p in self.root.glob('*.in'))

    @property
    def _missing_test_performance(self):
        """
        Debug utility: tests referenced in problem.props `performance` key,
        but with no corresponding .in and/or .out files.
        """
        return self._missing_test(self.properties.get_list('performance'))

    @property
    def _missing_test_samples(self):
        """
        Debug utility: tests referenced in problem.props `samples` key,
        but with no corresponding .in and/or .out files.
        """
        return self._missing_test(self.properties.get_list('samples'))

    @property
    def _missing_test_hidden(self):
        """
        Debug utility: tests referenced in problem.props `hidden` key,
        but with no corresponding .in and/or .out files.
        """
        return self._missing_test(self.properties.get_list('hidden'))

    @cached_property
    def samples(self):
        """The problem samples."""
        # Extract samples from self.tests
        # NOTE: don't use .with_suffix(), it removes all dotted parts
        samples = set(self.properties.get_list('samples'))
        for test in (test for test in self.tests if test.name in samples):
            comment = ''
            try:
                comment = self.read_file(test.name + '.comment')
            except FileNotFoundError:
                pass
            yield Sample(test.input, test.output, comment)

    @property
    def custom_check(self):
        path = self.properties.get('custom-check', '').strip()
        return self.root / Path(path) if path else None

    def is_custom_check_valid(self, test: Test, output, **kwargs) -> bool:
        """
        Check if a given `test` passes against `output` using the custom
        checker.
        **kwargs are passed verbatim to subprocess.check_call().

        May raise FileNotFoundError if checker program is not available.
        May raise other I/O errors if checker program fails at runtime.
        """
        assert self.custom_check, ("called run_custom_check() "
                                   "but there is no custom-check property")

        @contextlib.contextmanager
        def filename_for(data):
            # already a file-like object: return its name
            if hasattr(data, 'read') and hasattr(data, 'name'):
                data.seek(0)
                yield data.name
                return
            # source string: store it in a temporary file
            f = tempfile.NamedTemporaryFile(mode='w')
            f.write(data)
            f.flush()
            yield f.name
            # this will delete the temp file
            f.close()

        with ExitStack() as stack:
            # the checker program needs file paths as inputs;
            # create them using temporary files
            their_out_f, our_in_f, our_out_f = (
                stack.enter_context(filename_for(data))
                for data in (output, test.input, test.output))
            cmd = [str(self.custom_check), their_out_f, our_in_f, our_out_f]
            try:
                subprocess.check_call(
                    cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
                    **kwargs)
                return True
            except subprocess.SubprocessError:
                return False

    def test_passes(self, test: Test, output: str):
        """
        Check if a given `test` passes against `output`.
        """
        if self.custom_check:
            return self.is_custom_check_valid(test, output)
        return test.is_valid(output)
