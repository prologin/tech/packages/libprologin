import enum
from pathlib import Path

from libprologin import problems
from libprologin.contest import EventType
from libprologin.utils import cached_property


class Challenge(problems.Container):
    """
    A challenge is a list of problems, a subject and a type.
    A challenge is for a particular year and event type (qualification or
    regional event).
    """
    properties_filename = 'challenge.props'
    event_type_to_dirname = {
        EventType.qualification: 'qcm',
        EventType.regional: 'demi',
    }

    class Type(enum.Enum):
        standard = 'all-available'
        all_per_level = 'all-by-level'
        one_per_level = 'one-by-level'
        one_per_level_delayed = 'one-by-level-with-delay'

    def __init__(self, root: Path, year: int, event_type: EventType):
        self.name = '{}{}'.format(
            Challenge.event_type_to_dirname[event_type], year)
        self.root = root / self.name
        if not self.root.is_dir():
            raise FileNotFoundError("no such challenge: {}", self.root)
        self.year = year
        self.event_type = event_type

    def __repr__(self):
        return '<{}: {} {}>'.format(
            self.__class__.__name__, self.year, self.event_type.name)

    def __hash__(self):
        return hash(self.root)

    def __eq__(self, other):
        return self.root == other.root

    def __lt__(self, other):
        return (self.year, self.event_type) < (other.year, other.event_type)

    @cached_property
    def problems(self):
        from libprologin.problems.problem import Problem

        for path in self.root.iterdir():
            if path.is_dir():
                try:
                    yield Problem(self, path.name)
                except FileNotFoundError:  # noqa
                    pass

    @cached_property
    def _problem_dict(self):
        return {problem.name: problem for problem in self.problems}

    def problem_by_name(self, name: str):
        return self._problem_dict[name]

    @property
    def type(self):
        return Challenge.Type(self.properties.get('type'))

    @property
    def public(self):
        return self.properties.get_bool('display_website')

    @property
    def auto_unlock_delay(self):
        return self.properties.get_int('unlock_delay')
