from libprologin.problems.properties import Properties
from libprologin.utils import cached_property


class Container:
    properties_filename = None

    def read_file(self, filename):
        with (self.root / filename).open() as f:
            return f.read()

    @cached_property
    def properties(self):
        return Properties.parse(self.root / self.properties_filename)
