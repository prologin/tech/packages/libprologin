from collections.abc import Mapping
from pathlib import Path


class Properties(Mapping):
    def __init__(self, mapping: dict):
        self.mapping = mapping

    def __len__(self):
        return len(self.mapping)

    def __iter__(self):
        return iter(self.mapping)

    def __getitem__(self, item):
        return self.mapping[item.replace('_', '-')]

    def get_bool(self, item, default=False) -> bool:
        try:
            return self[item].lower() == 'true'
        except KeyError:
            return default

    def get_int(self, item, default=0):
        try:
            return int(self[item])
        except KeyError:
            return default

    def get_float(self, item, default=0):
        try:
            return float(self[item])
        except KeyError:
            return default

    def get_list(self, item, default=None):
        try:
            return self[item].split()
        except KeyError:
            return default or []

    @classmethod
    def parse(cls, path: Path):
        def items():
            with path.open() as f:
                for line in f:
                    line = line.strip()
                    if not line or line.startswith('#'):
                        continue
                    key, value = line.split(':', 1)
                    yield key.strip().replace('_', '-'), value.strip()
        return cls(dict(items()))
