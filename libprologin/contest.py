import enum


class EventType(enum.IntEnum):
    qualification = 0
    regional = 1
    final = 2
