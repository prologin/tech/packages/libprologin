import enum
from gettext import gettext as _

identity = lambda e: e  # noqa


def e(*args, **kwargs):
    for arg in ('doc', 'camisole', 'time', 'memory'):
        args += (kwargs.get(arg, None),)
    return args


class Language(enum.Enum):
    """
    A programming language.
    """
    c = e("C", ['.c'], doc='c', camisole='c', memory=lambda m: m * 10 + 1024)
    cpp = e("C++", ['.cc', '.c++', '.cpp'], doc='cpp', camisole='c++',
            memory=lambda m: m * 10 + 3000)
    pascal = e("Pascal", ['.pas', '.pascal'], doc='pascal', camisole='pascal',
               memory=lambda m: m + 1024)
    ocaml = e("OCaml", ['.ml', '.ocaml'], doc='ocaml', camisole='ocaml',
              memory=lambda m: 2 * m + 1024)
    scheme = e("Scheme", ['.scm'], doc='scheme', camisole='scheme',
               memory=lambda m: m + 36384, time=lambda t: 3 * t)
    haskell = e("Haskell", ['.hs'], doc='haskell', camisole='haskell',
                memory=lambda m: 5 * m + 15000, time=lambda t: 4 * t + 1024)
    java = e("Java", ['.java'], doc='java', camisole='java',
             memory=lambda m: 5 * m + 22000, time=lambda t: 4 * t + 36000)
    python = e("Python", ['.py', '.py3'], doc='python', camisole='python',
               memory=lambda m: 5 * m + 7500, time=lambda t: 15 * t)
    ada = e("Ada", ['.adb'], doc='ada', camisole='ada',
            memory=lambda m: m + 4096)
    php = e("PHP", ['.php'], doc='php', camisole='php',
            memory=lambda m: 5 * m + 36384, time=lambda t: 8 * t)
    js = e("Javascript", ['.js'], camisole='javascript',
           memory=lambda m: 5 * m + 100000, time=lambda t: 5 * t)
    vb = e("Visual Basic", ['.vb'], camisole='visualbasic',
           memory=lambda m: m + 16384, time=lambda t: 2 * t)
    perl = e("Perl", ['.pl', '.perl'], camisole='perl',
             memory=lambda m: m + 5000, time=lambda t: 10 * t)
    lua = e("Lua", ['.lua'], camisole='lua', memory=lambda m: m + 5000,
            time=lambda t: 10 * t)
    csharp = e("C#", ['.cs'], camisole='c#', memory=lambda m: m + 16384,
               time=lambda t: t + 38)
    fsharp = e("F#", ['.fs'], camisole='f#', memory=lambda m: m + 17408)
    brainfuck = e("Brainfuck", ['.bf'], camisole='brainfuck',
                  memory=lambda m: 10 * m + 50000, time=lambda t: 8 * t)
    pseudocode = e(_("Pseudocode"), ['.txt'])

    def __init__(self, display_name, extensions, doc=None, camisole=None,
                 time=None, memory=None):
        """
        :param display_name: the language full human name
        :param extensions: file extension(s) accepted for this language
        :param camisole: the corresponding camisole language name
               (optional, if correctable)
        :param doc: the documentation name for this language (optional)
        :param time: a callable giving the maximum user time limit given the raw
               user time limit (optional)
        :param memory: a callable giving the maximum memory limit given the raw
               memory limit (optional)
        """
        self.display_name = display_name
        self.extensions = extensions
        self.doc = doc
        self.camisole_name = camisole
        self.time_limit = time or identity
        # Hardcoded increase to account for various external factors and because
        # we don't want those limits to be really tight, we mainly want to avoid
        # really consuming programs asymptotically. Therefore, we always add
        # 4MB of padding memory and multiply the displayed constraint by two.
        memory = memory or identity
        self.memory_limit = lambda x: 4096 + memory(x) * 2

    def __str__(self):
        return str(self.display_name)  # force gettext resolution

    def __repr__(self):
        return '<{} {}>'.format(self.__class__.__name__, self)

    @property
    def correctable(self):
        return self.camisole_name is not None

    @classmethod
    def guess(cls, obj):
        if isinstance(obj, cls):
            return obj
        # assume string
        obj = obj.lower().strip()
        # some special cases
        obj = {
            'caml': 'ocaml',
        }.get(obj, obj)
        try:
            # 'cpp', 'PYTHON'
            return cls[obj]
        except KeyError:
            pass
        for lang in cls:
            # try with human name
            if lang.display_name.lower() == obj:
                return lang
            # try with extensions
            if obj in lang.extensions or '.' + obj in lang.extensions:
                return lang
        return None

    @classmethod
    def _get_choices(cls):
        # We hacked the member values to put LanguageDef-s instead of DB values
        # so we fix this here.
        # (("c", "C"), ("cpp", "C++"), ("csharp", "C#"), …)
        return tuple((member.name, member.display_name) for member in cls)
