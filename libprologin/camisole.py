import requests

from libprologin.language import Language
from libprologin.problems.problem import Problem


def exited_cleanly(data: dict) -> bool:
    """
    Check if a camisole output dict exited cleanly.

    :param data: a camisole test output dict
    """
    return data['exitcode'] == 0 and data['meta']['status'] == 'OK'


def send(url: str, problem: Problem, lang: Language, source: str, opts=None,
         **kwargs):
    """
    Send a run request to a camisole HTTP runner.

    :param url: url of the camisole runner
    :param problem: Problem to generate tests for
    :param lang: Language of the tested program
    :param source: tested program source
    :param opts: extra camisole options (dict, optional)
    :param kwargs: passed verbatim to requests.post()
    :return: the verbatim camisole output
    """
    data = {
        'lang': lang.camisole_name,
        'source': source,
        'tests': [
            {'name': test.name, 'stdin': test.input} for test in problem.tests
        ],
    }
    data.update(opts or {})
    return requests.post(url, json=data, **kwargs).json()
