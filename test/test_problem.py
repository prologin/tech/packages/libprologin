import pytest

from libprologin.language import Language
from libprologin.problems.problem import Problem, TestType
from libprologin.problems.repository import Repository


def test_not_a_dir(repository: Repository):
    c = repository.challenge_by_name('demi2017')

    with pytest.raises(FileNotFoundError):
        Problem(c, 'challenge.props')


def test_attributes(repository: Repository):
    c = repository.challenge_by_name('demi2017')
    problems = list(c.problems)
    assert len(problems) == 2

    assert problems[0].name == 'my-problem'
    assert problems[0].title == "Whatever title"
    assert problems[0].difficulty == 1
    assert problems[0].custom_check is None
    assert problems[0].subject_html is None
    assert problems[0].subject_markdown == '# The subject'
    assert problems[0].subject == '# The subject'
    assert problems[0].stop_early is False

    assert problems[0]._missing_test_in == {'noin'}
    assert problems[0]._missing_test_out == {'noout'}
    assert problems[0]._missing_test_hidden == {'03'}
    assert problems[0]._missing_test_performance == {'04'}
    assert problems[0]._missing_test_samples == {'05'}

    assert problems[1].name == 'other-problem'
    assert problems[1].title == "Iron Man: the return"
    assert problems[1].difficulty == 2
    assert problems[1].custom_check is not None
    assert problems[1].subject_markdown is None
    assert problems[1].subject_html == '<h1>The subject</h1>'
    assert problems[1].subject == '<h1>The subject</h1>'
    assert problems[1].maximum_memory == 1337
    assert problems[1].maximum_time == 42.
    assert problems[1].stop_early is True


def test_samples(repository: Repository):
    c = repository.challenge_by_name('demi2017')
    p = c.problem_by_name('my-problem')
    assert len(p.samples) == 2
    assert p.samples[0].input == '2'
    assert p.samples[0].output == '84'
    assert p.samples[0].comment == 'chiche'
    assert p.samples[1].comment == ''


def test_references(repository: Repository):
    c = repository.challenge_by_name('demi2017')
    p = c.problem_by_name('other-problem')
    assert len(p.references) == 1
    lang, path = p.references[0]
    assert lang is Language.c
    assert path.open().read() == 'int main(void) { return 0; }'


def test_tests(repository: Repository):
    c = repository.challenge_by_name('demi2017')
    p = c.problem_by_name('my-problem')

    t = p.test_by_name('some-test.lol')
    assert t.hidden
    assert t.type == TestType.correction

    t = p.test_by_name('02')
    assert not t.hidden
    assert t.type == TestType.performance


def test_equality(repository: Repository):
    c = repository.challenge_by_name('demi2017')
    assert (c.problem_by_name('my-problem') ==
            c.problem_by_name('my-problem'))


def test_ordering(repository: Repository):
    c = repository.challenge_by_name('demi2017')
    assert (c.problem_by_name('my-problem') <
            c.problem_by_name('other-problem'))


def test_test_passes(repository: Repository, tmpdir):
    c = repository.challenge_by_name('demi2017')
    p = c.problem_by_name('my-problem')
    assert len(p.tests) == 2
    assert p.test_passes(p.tests[0], ' 84   ')
    assert not p.test_passes(p.tests[0], 'nope')
    assert p.test_passes(p.tests[1], '   42 ')
    assert not p.test_passes(p.tests[1], 'nope')

    p = c.problem_by_name('other-problem')
    assert p.custom_check
    assert len(p.tests) == 1
    test = p.tests[0]
    assert not p.test_passes(test, 'abcdef!')  # superfluous '!'
    assert p.test_passes(test, 'abcdef')
    assert p.test_passes(test, 'defabc')

    f = tmpdir.ensure("bad")
    f.write('abcdef!')
    assert not p.test_passes(test, f.open('r'))
    f = tmpdir.ensure("good")
    f.write('abcdef')
    assert p.test_passes(test, f.open('r'))
