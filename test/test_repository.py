from libprologin.problems.repository import Repository
from libprologin.contest import EventType


def test_all_challenges(problems_path, problems_year):
    r = Repository(problems_path)

    expected = set(
        (year, event_type) for year in range(2002, problems_year + 1)
        for event_type in (EventType.qualification, EventType.regional))

    actual = set((c.year, c.event_type) for c in r.challenges())

    assert actual == expected
