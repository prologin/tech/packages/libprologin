import pytest

from libprologin.contest import EventType
from libprologin.problems.challenge import Challenge
from libprologin.problems.repository import Repository


def test_not_a_dir(repository: Repository):
    with pytest.raises(FileNotFoundError):
        repository.challenge_by_name('qcm2016')


def test_missing_props(repository: Repository):
    c = repository.challenge_by_name('qcm2017')
    with pytest.raises(FileNotFoundError):
        c.public


def test_main_attributes(repository: Repository):
    challenge = repository.challenge_by_name('demi2017')
    assert challenge.year == 2017
    assert challenge.auto_unlock_delay == 42
    assert challenge.event_type == EventType.regional


def test_properties(repository: Repository):
    challenge = repository.challenge_by_name('demi2017')
    assert challenge.properties['title'] == "Régionale 2017, épreuve machine"
    assert challenge.properties['category'] == "régionale"
    assert challenge.properties['year'] == "2017"
    assert challenge.properties['display_website'] == "false"
    assert challenge.properties['type'] == "one-by-level-with-delay"
    assert challenge.type == Challenge.Type.one_per_level_delayed


def test_equality(repository: Repository):
    assert (repository.challenge_by_name('qcm2017') ==
            repository.challenge_by_name('qcm2017'))
    assert (repository.challenge_by_name('demi2017') ==
            repository.challenge_by_type_year(2017, EventType.regional))


def test_ordering(repository: Repository):
    assert (repository.challenge_by_name('demi2016') <
            repository.challenge_by_name('qcm2017') <
            repository.challenge_by_name('demi2017'))
