from libprologin.problems.properties import Properties


def test_iter(properties: Properties):
    assert len(properties) == 11

    for key in properties:
        # dummy test to cover iter
        assert ':' not in key


def test_get(properties: Properties):
    assert properties.get('nope') is None
    assert properties.get('foo') == 'bar2'
    assert properties.get('some_key') == '1'
    assert properties.get('some-key') == '1'
    assert properties.get('some_other') == '2'
    assert properties.get('some-other') == '2'


def test_get_int(properties: Properties):
    assert properties.get_int('myint') == 987654321
    assert properties.get_int('nope') == 0


def test_get_float(properties: Properties):
    assert properties.get_float('myfloat') == 12345.67890
    assert properties.get_float('nope') == 0.


def test_get_list(properties: Properties):
    assert properties.get_list('mylist') == ['a', 'b', 'c', 'd']
    assert properties.get_list('nope') == []


def test_get_bool(properties: Properties):
    assert properties.get_bool('bool1') is False
    assert properties.get_bool('bool2') is True
    assert properties.get_bool('bool3') is True
    assert properties.get_bool('bool4') is False
    assert properties.get_bool('nope') is False
