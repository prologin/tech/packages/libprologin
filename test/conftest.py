from pathlib import Path

import pytest

from libprologin.problems import Properties


def pytest_addoption(parser):
    parser.addoption("--problems", help="path to problems/ repository")
    parser.addoption("--year", help="last year")


@pytest.fixture
def problems_path(request):
    path = Path(request.config.getoption("--problems"))
    print("Using problems/ at:", path)
    return path


@pytest.fixture
def problems_year(request):
    return int(request.config.getoption("--year"))


@pytest.fixture
def repository():
    from libprologin.problems.repository import Repository
    return Repository(Path(__file__).parent / 'dummy')


@pytest.fixture
def properties():
    return Properties.parse(Path(__file__).parent / 'dummy' / 'test.props')
