#!/usr/bin/env python

import sys

if __name__ == '__main__':
    # their out, our in, our out
    ok = sorted(open(sys.argv[1]).read()) == sorted(open(sys.argv[2]).read())
    sys.exit(0 if ok else 1)
