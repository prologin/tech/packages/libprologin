from libprologin.language import Language


def test_choices():
    choices = Language._get_choices()
    assert ('c', 'C') in choices
    assert ('vb', 'Visual Basic') in choices


def test_basic_properties():
    assert Language.ada.name == 'ada'
    assert Language.ada.display_name == 'Ada'
    assert Language.ada.camisole_name == 'ada'
    assert Language.ada.extensions == ['.adb']
    assert Language.ada.correctable

    # c: m * 10 + 1024
    # margin: m * 2 + 4096
    assert Language.c.memory_limit(0) == (0 * 10 + 1024) * 2 + 4096
    assert Language.c.memory_limit(5) == (5 * 10 + 1024) * 2 + 4096

    assert Language.csharp.name == 'csharp'
    assert Language.csharp.display_name == 'C#'
    assert Language.csharp.camisole_name == 'c#'
    assert Language.csharp.extensions == ['.cs']
    assert Language.csharp.correctable

    assert not Language.pseudocode.correctable

    assert str(Language.vb) == 'Visual Basic'


def test_guess():
    assert Language.guess('c') == Language.c
    assert Language.guess('.c') == Language.c
    assert Language.guess('.C') == Language.c
    assert Language.guess(' .c ') == Language.c
    assert Language.guess('cpp') == Language.cpp
    assert Language.guess('.cpp') == Language.cpp
    assert Language.guess('C++') == Language.cpp
    assert Language.guess('.py') == Language.python
    assert Language.guess('.py3') == Language.python
    assert Language.guess('py3') == Language.python
    assert Language.guess('.py2') is None  # haters gonna hate
    assert Language.guess('vb') == Language.vb
    assert Language.guess('.vb') == Language.vb
    assert Language.guess('visual basic') == Language.vb
    assert Language.guess('.txt') == Language.pseudocode
    assert Language.guess(Language.python) == Language.python
